#!/usr/bin/python3
"""Setuptools description for vtrx_register_maps"""

import setuptools

setuptools.setup(
    name="vtrx_register_maps",
    version="1.0.0",
    description="VTRx+ register maps as class",
    author="Roman Mueller, University of Bern",
    packages=["vtrx_register_maps"],
    include_package_data=True,
)
