# VTRx+ quad laser driver register map classes
Supported versions:
- v1.2
- v1.3
All information taken from [EDMS](https://edms.cern.ch/ui/#!master/navigator/document?P:1930058715:1274960871:subDocs)

Inspired by the [lpgbt_control_lib](https://gitlab.cern.ch/lpgbt/lpgbt_control_lib) register maps.

Feel free to use/fork, cite Bern ATLAS group :)