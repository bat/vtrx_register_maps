#!/usr/bin/env python3

# VTRx+ quad LD v1.2 register map inspired by lpgbt_control_lib
# author: Roman Mueller, Bern


"""VTRx+ quad LD v1.2 Constants"""

from enum import IntEnum, unique


class VTRxplusRegisterMapV1_2:
    """Class containing all VTRxplus-related constants"""

    class GCR:
        """Global Control Register"""

        address = 0x00

        @staticmethod
        def __str__():
            return "GCR"

        @staticmethod
        def __int__():
            return 0x00

        class GPEN:
            """Global Pre-emphasis Enable: Global enable of the pre-emphasis circuit. The amplitude of the pre-emphasis is controlled by EMPAMP."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class GMEN:
            """Global Modulation Circuit Enable: Global enable of the modulation current generator. This bit is masked by the GCEN bit."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class GBEN:
            """Global Bias Circuit Enable: Global enable of the bias current generator. This bit is masked by the GCEN bit."""

            offset = 2
            length = 1
            bit_mask = 4
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class GLAEN:
            """Global Limiting Amplifier Enable: Global enable of the limiting amplifier. This bit is masked by the GCEN bit."""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class GCEN:
            """Global Chip Enable: This signal is ANDed with the inverted DIS signal coming from the pad. To enable the analog circuits in the device, this bit has to be set to one and DIS signal has to be low (or left floating)."""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class ODC:

        address = 0x01

        @staticmethod
        def __str__():
            return "ODC"

        @staticmethod
        def __int__():
            return 0x01

        class DS:
            """???"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class C0CR:

        address = 0x04

        @staticmethod
        def __str__():
            return "C0CR"

        @staticmethod
        def __int__():
            return 0x04

        class C0FEP:
            """Channel 0 Falling Edge Pre-emphasis: value of 1 enables falling edge preemphasis on channel 0. This bit is masked by the GPEN bit in the GCR."""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C0REP:
            """Channel 0 Rising Edge Pre-emphasis: value of 1 enables rising edge preemphasis on channel 0. This bit is masked by the GPEN bit in the GCR."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C0MEN:
            """Channel 0 Modulation Circuit Enable: value of 1 enables the modulation current of channel 0. This bit is masked by the GMEN bit in the GCR."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C0BEN:
            """Channel 0 Biasing Circuit Enable: value of 1 enables the bias current generator of channel 0. This bit is masked by the GBEN bit in the GCR."""

            offset = 2
            length = 1
            bit_mask = 4
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C0LAEN:
            """Channel 0 Limiting Amplifier Enable: value of 1 enables the limiting amplifier of channel 0. This bit is masked by the GLAEN bit in the GCR."""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C0CEN:
            """Channel 0 enable: value of 0 disables channel 0. This bit is masked by the GCEN bit in the GCR."""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class C0BC:

        address = 0x05

        @staticmethod
        def __str__():
            return "C0BC"

        @staticmethod
        def __int__():
            return 0x05

        class C0BiasCur:
            """Biasing current step size is given in specification # 4.3.2. The bias generator is enabled by setting the C0BEN bit in the C0CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x2f

            @staticmethod
            def validate(value):
                return value in range(128)

    class C0MC:

        address = 0x06

        @staticmethod
        def __str__():
            return "C0MC"

        @staticmethod
        def __int__():
            return 0x06

        class C0ModCur:
            """Modulation current step size is given in specification # 4.4.6. The modulation current generator is enabled by setting the C0MEN bit in the C0CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x26

            @staticmethod
            def validate(value):
                return value in range(128)

    class C0EA:

        address = 0x07

        @staticmethod
        def __str__():
            return "C0EA"

        @staticmethod
        def __int__():
            return 0x07

        class C0EmpAmp:
            """Pre-emphasis amplitude. To enable pre-emphasis at least one of FEP or REP in CNCR register must be set (as well as GPE bit in GCR)"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)

    class C1CR:

        address = 0x08

        @staticmethod
        def __str__():
            return "C1CR"

        @staticmethod
        def __int__():
            return 0x08

        class C1FEP:
            """channel 1 Falling Edge Pre-emphasis: value of 1 enables falling edge preemphasis on channel 1. This bit is masked by the GPEN bit in the GCR."""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C1REP:
            """channel 1 Rising Edge Pre-emphasis: value of 1 enables rising edge preemphasis on channel 1. This bit is masked by the GPEN bit in the GCR."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C1MEN:
            """channel 1 Modulation Circuit Enable: value of 1 enables the modulation current of channel 1. This bit is masked by the GMEN bit in the GCR."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C1BEN:
            """channel 1 Biasing Circuit Enable: value of 1 enables the bias current generator of channel 1. This bit is masked by the GBEN bit in the GCR."""

            offset = 2
            length = 1
            bit_mask = 4
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C1LAEN:
            """channel 1 Limiting Amplifier Enable: value of 1 enables the limiting amplifier of channel 1. This bit is masked by the GLAEN bit in the GCR."""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C1CEN:
            """channel 1 enable: value of 0 disables channel 1. This bit is masked by the GCEN bit in the GCR."""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class C1BC:

        address = 0x09

        @staticmethod
        def __str__():
            return "C1BC"

        @staticmethod
        def __int__():
            return 0x09

        class C1BiasCur:
            """Biasing current step size is given in specification # 4.3.2. The bias generator is enabled by setting the C1BEN bit in the C1CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x2f

            @staticmethod
            def validate(value):
                return value in range(128)

    class C1MC:

        address = 0x0a

        @staticmethod
        def __str__():
            return "C1MC"

        @staticmethod
        def __int__():
            return 0x0a

        class C1ModCur:
            """Modulation current step size is given in specification # 4.4.6. The modulation current generator is enabled by setting the C1MEN bit in the C1CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x26

            @staticmethod
            def validate(value):
                return value in range(128)

    class C1EA:

        address = 0x0b

        @staticmethod
        def __str__():
            return "C1EA"

        @staticmethod
        def __int__():
            return 0x0b

        class C1EmpAmp:
            """Pre-emphasis amplitude. To enable pre-emphasis at least one of FEP or REP in CNCR register must be set (as well as GPE bit in GCR)"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)


    class C2CR:

        address = 0x0c

        @staticmethod
        def __str__():
            return "C2CR"

        @staticmethod
        def __int__():
            return 0x0c

        class C2FEP:
            """channel 2 Falling Edge Pre-emphasis: value of 1 enables falling edge preemphasis on channel 2. This bit is masked by the GPEN bit in the GCR."""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C2REP:
            """channel 2 Rising Edge Pre-emphasis: value of 1 enables rising edge preemphasis on channel 2. This bit is masked by the GPEN bit in the GCR."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C2MEN:
            """channel 2 Modulation Circuit Enable: value of 1 enables the modulation current of channel 2. This bit is masked by the GMEN bit in the GCR."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C2BEN:
            """channel 2 Biasing Circuit Enable: value of 1 enables the bias current generator of channel 2. This bit is masked by the GBEN bit in the GCR."""

            offset = 2
            length = 1
            bit_mask = 4
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C2LAEN:
            """channel 2 Limiting Amplifier Enable: value of 1 enables the limiting amplifier of channel 2. This bit is masked by the GLAEN bit in the GCR."""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C2CEN:
            """channel 2 enable: value of 0 disables channel 2. This bit is masked by the GCEN bit in the GCR."""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class C2BC:

        address = 0x0d

        @staticmethod
        def __str__():
            return "C2BC"

        @staticmethod
        def __int__():
            return 0x0d

        class C2BiasCur:
            """Biasing current step size is given in specification # 4.3.2. The bias generator is enabled by setting the C2BEN bit in the C2CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x2f

            @staticmethod
            def validate(value):
                return value in range(128)

    class C2MC:

        address = 0x0e

        @staticmethod
        def __str__():
            return "C2MC"

        @staticmethod
        def __int__():
            return 0x0e

        class C2ModCur:
            """Modulation current step size is given in specification # 4.4.6. The modulation current generator is enabled by setting the C2MEN bit in the C2CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x26

            @staticmethod
            def validate(value):
                return value in range(128)

    class C2EA:

        address = 0x0f

        @staticmethod
        def __str__():
            return "C2EA"

        @staticmethod
        def __int__():
            return 0x0f

        class C2EmpAmp:
            """Pre-emphasis amplitude. To enable pre-emphasis at least one of FEP or REP in CNCR register must be set (as well as GPE bit in GCR)"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)


    class C3CR:

        address = 0x10

        @staticmethod
        def __str__():
            return "C3CR"

        @staticmethod
        def __int__():
            return 0x10

        class C3FEP:
            """channel 3 Falling Edge Pre-emphasis: value of 1 enables falling edge preemphasis on channel 3. This bit is masked by the GPEN bit in the GCR."""

            offset = 5
            length = 1
            bit_mask = 32
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C3REP:
            """channel 3 Rising Edge Pre-emphasis: value of 1 enables rising edge preemphasis on channel 3. This bit is masked by the GPEN bit in the GCR."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class C3MEN:
            """channel 3 Modulation Circuit Enable: value of 1 enables the modulation current of channel 3. This bit is masked by the GMEN bit in the GCR."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C3BEN:
            """channel 3 Biasing Circuit Enable: value of 1 enables the bias current generator of channel 3. This bit is masked by the GBEN bit in the GCR."""

            offset = 2
            length = 1
            bit_mask = 4
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C3LAEN:
            """channel 3 Limiting Amplifier Enable: value of 1 enables the limiting amplifier of channel 3. This bit is masked by the GLAEN bit in the GCR."""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class C3CEN:
            """channel 3 enable: value of 0 disables channel 3. This bit is masked by the GCEN bit in the GCR."""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class C3BC:

        address = 0x11

        @staticmethod
        def __str__():
            return "C3BC"

        @staticmethod
        def __int__():
            return 0x11

        class C3BiasCur:
            """Biasing current step size is given in specification # 4.3.2. The bias generator is enabled by setting the C3BEN bit in the C3CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x2f

            @staticmethod
            def validate(value):
                return value in range(128)

    class C3MC:

        address = 0x12

        @staticmethod
        def __str__():
            return "C3MC"

        @staticmethod
        def __int__():
            return 0x12

        class C3ModCur:
            """Modulation current step size is given in specification # 4.4.6. The modulation current generator is enabled by setting the C3MEN bit in the C3CR register (as well as corresponding bits in GCR)"""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x26

            @staticmethod
            def validate(value):
                return value in range(128)

    class C3EA:

        address = 0x13

        @staticmethod
        def __str__():
            return "C3EA"

        @staticmethod
        def __int__():
            return 0x13

        class C3EmpAmp:
            """Pre-emphasis amplitude. To enable pre-emphasis at least one of FEP or REP in CNCR register must be set (as well as GPE bit in GCR)"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)

    class CCNF:
        """The CCNF affects the clock generation used for register immunity to Single Event Effects. It is intended to be used only during initial testing and its value should therefore not be changed during normal operation. For more details please refer to section 7.1. !Warning! Please note that disabling more than one clock will cause the chip to stop responding. To recover the chip a power cycle or sending RSTN signal is necessary."""

        address = 0x1c

        @staticmethod
        def __str__():
            return "CCNF"

        @staticmethod
        def __int__():
            return 0x1c

        class CSEN:
            """Clock spying enable. Setting this bit to one does not trigger clock spying for current transaction. To start clock spying one has to write to register CSE"""

            offset = 7
            length = 1
            bit_mask = 128
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CEC:
            """Clock enable for clock C"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CEB:
            """Clock enable for clock B"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CEA:
            """Clock enable for clock A"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class PORS:
        """Power-on reset status register"""

        address = 0x1d

        @staticmethod
        def __str__():
            return "PORS"

        @staticmethod
        def __int__():
            return 0x1d

        class PORC:
            """Power on reset C"""

            offset = 2
            length = 1
            bit_mask = 4

            @staticmethod
            def validate(value):
                return value in range(2)

        class PORB:
            """Power on reset B"""

            offset = 1
            length = 1
            bit_mask = 2

            @staticmethod
            def validate(value):
                return value in range(2)

        class PORA:
            """Power on reset A"""

            offset = 0
            length = 1
            bit_mask = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class SEUC:
        """Single Event Upset counter register"""

        address = 0x1e

        @staticmethod
        def __str__():
            return "SEUC"

        @staticmethod
        def __int__():
            return 0x1e

        class SEUCntr:
            """Counter containing the integrated number of SEU events. Writing to the counter resets it to zero."""

            offset = 0
            length = 8
            bit_mask = 255
            default = 0

            @staticmethod
            def validate(value):
                return value in range(256)

    class CSE:
        """Clock Spying enable register"""

        address = 0x1f

        @staticmethod
        def __str__():
            return "CSE"

        @staticmethod
        def __int__():
            return 0x1f

        class ClkSpyEna:
            """Writing to this register can trigger clock spying event (see section 7.1)."""

            offset = 0
            length = 8
            bit_mask = 255
            default = 0

            @staticmethod
            def validate(value):
                return value in range(256)

    @unique
    class Reg(IntEnum):
        GCR = 0x00
        ODC = 0x01
        C0CR = 0x04
        C0BC = 0x05
        C0MC = 0x06
        C0EA = 0x07
        C1CR = 0x08
        C1BC = 0x09
        C1MC = 0x0a
        C1EA = 0x0b
        C2CR = 0x0c
        C2BC = 0x0d
        C2MC = 0x0e
        C2EA = 0x0f
        C3CR = 0x10
        C3BC = 0x11
        C3MC = 0x12
        C3EA = 0x13
        CCNF = 0x1c
        PORS = 0x1d
        SEUC = 0x1e
        CSE = 0x1f
