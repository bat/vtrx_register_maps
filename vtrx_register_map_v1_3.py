#!/usr/bin/env python3

# VTRx+ quad LD v1.3 register map inspired by lpgbt_control_lib
# author: Roman Mueller, Bern


"""VTRx+ quad LD v1.3 Constants"""

from enum import IntEnum, unique


class VTRxplusRegisterMapV1_3:
    """Class containing all VTRxplus-related constants"""

    class GCR:
        """Global Control Register: Note that it is not possible to completely turn off any given channel, there will always be a bias current corresponding to 1 LSB flowing in the output of the LDQ10. This is to ensure chip reliability."""

        address = 0x00

        @staticmethod
        def __str__():
            return "GCR"

        @staticmethod
        def __int__():
            return 0x00

        class CH4EN:
            """Channel 4 Enable."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH3EN:
            """Channel 3 Enable."""

            offset = 2
            length = 1
            bit_mask = 4
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH2EN:
            """Channel 2 Enable."""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH1EN:
            """Channel 1 Enable."""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class SDACNT:
        """SDA Control Register"""

        address = 0x01

        @staticmethod
        def __str__():
            return "SDACNT"

        @staticmethod
        def __int__():
            return 0x01

        class DRVSDA:
            """Drive SDA. Do not change unless advised to do so by the support team."""

            offset = 1
            length = 1
            bit_mask = 2
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class SDADS:
            """SDA Driving Strength (0-low, 1-high). Could be lowered to try to reduce rise times and their associated EMI."""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH1BIAS:
        """Channel 1 Bias Current Register"""

        address = 0x03

        @staticmethod
        def __str__():
            return "CH1BIAS"

        @staticmethod
        def __int__():
            return 0x03

        class CH1Bias:
            """Biasing current step size is given in specification # 4.3.2. Note that setting 0 and 1 are functionally equivalent and correspond to 1 unit of the bias current step. The bias generator is enabled by setting the corresponding channel enable bit in the GCR."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x30

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH1MOD:
        """Channel 1 Modulation Current Register"""

        address = 0x04

        @staticmethod
        def __str__():
            return "CH1MOD"

        @staticmethod
        def __int__():
            return 0x04

        class CH1MODEN:
            """Modulation current generator enable."""

            offset = 7
            length = 1
            bit_mask = 255
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH1MOD:
            """Modulation current step size is given in specification # 4.4.6."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x20

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH1EMP:
        """Channel 1 Emphasis Amplitude Register"""

        address = 0x05

        @staticmethod
        def __str__():
            return "CH1EMP"

        @staticmethod
        def __int__():
            return 0x05

        class CH1EMPF:
            """Falling edge pre-emphasis enable."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH1EMPR:
            """Rising edge pre-emphasis enable."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH1EMPAMP:
            """Pre-emphasis amplitude."""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH2BIAS:
        """Channel 2 Bias Current Register"""

        address = 0x06

        @staticmethod
        def __str__():
            return "CH2BIAS"

        @staticmethod
        def __int__():
            return 0x06

        class CH2Bias:
            """Biasing current step size is given in specification # 4.3.2. Note that setting 0 and 1 are functionally equivalent and correspond to 1 unit of the bias current step. The bias generator is enabled by setting the corresponding channel enable bit in the GCR."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x30

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH2MOD:
        """Channel 2 Modulation Current Register"""

        address = 0x07

        @staticmethod
        def __str__():
            return "CH2MOD"

        @staticmethod
        def __int__():
            return 0x07

        class CH2MODEN:
            """Modulation current generator enable."""

            offset = 7
            length = 1
            bit_mask = 255
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH2MOD:
            """Modulation current step size is given in specification # 4.4.6."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x20

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH2EMP:
        """Channel 2 Emphasis Amplitude Register"""

        address = 0x08

        @staticmethod
        def __str__():
            return "CH2EMP"

        @staticmethod
        def __int__():
            return 0x08

        class CH2EMPF:
            """Falling edge pre-emphasis enable."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH2EMPR:
            """Rising edge pre-emphasis enable."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH2EMPAMP:
            """Pre-emphasis amplitude."""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH3BIAS:
        """Channel 3 Bias Current Register"""

        address = 0x09

        @staticmethod
        def __str__():
            return "CH3BIAS"

        @staticmethod
        def __int__():
            return 0x09

        class CH3Bias:
            """Biasing current step size is given in specification # 4.3.2. Note that setting 0 and 1 are functionally equivalent and correspond to 1 unit of the bias current step. The bias generator is enabled by setting the corresponding channel enable bit in the GCR."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x30

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH3MOD:
        """Channel 3 Modulation Current Register"""

        address = 0x0a

        @staticmethod
        def __str__():
            return "CH3MOD"

        @staticmethod
        def __int__():
            return 0x0a

        class CH3MODEN:
            """Modulation current generator enable."""

            offset = 7
            length = 1
            bit_mask = 255
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH3MOD:
            """Modulation current step size is given in specification # 4.4.6."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x20

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH3EMP:
        """Channel 3 Emphasis Amplitude Register"""

        address = 0x0b

        @staticmethod
        def __str__():
            return "CH3EMP"

        @staticmethod
        def __int__():
            return 0x0b

        class CH3EMPF:
            """Falling edge pre-emphasis enable."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH3EMPR:
            """Rising edge pre-emphasis enable."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH3EMPAMP:
            """Pre-emphasis amplitude."""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH4BIAS:
        """Channel 4 Bias Current Register"""

        address = 0x0c

        @staticmethod
        def __str__():
            return "CH4BIAS"

        @staticmethod
        def __int__():
            return 0x0c

        class CH4Bias:
            """Biasing current step size is given in specification # 4.3.2. Note that setting 0 and 1 are functionally equivalent and correspond to 1 unit of the bias current step. The bias generator is enabled by setting the corresponding channel enable bit in the GCR."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x30

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH4MOD:
        """Channel 4 Modulation Current Register"""

        address = 0x0d

        @staticmethod
        def __str__():
            return "CH4MOD"

        @staticmethod
        def __int__():
            return 0x0d

        class CH4MODEN:
            """Modulation current generator enable."""

            offset = 7
            length = 1
            bit_mask = 255
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH4MOD:
            """Modulation current step size is given in specification # 4.4.6."""

            offset = 0
            length = 7
            bit_mask = 127
            default = 0x20

            @staticmethod
            def validate(value):
                return value in range(128)

    class CH4EMP:
        """Channel 4 Emphasis Amplitude Register"""

        address = 0x0e

        @staticmethod
        def __str__():
            return "CH4EMP"

        @staticmethod
        def __int__():
            return 0x0e

        class CH4EMPF:
            """Falling edge pre-emphasis enable."""

            offset = 4
            length = 1
            bit_mask = 16
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)


        class CH4EMPR:
            """Rising edge pre-emphasis enable."""

            offset = 3
            length = 1
            bit_mask = 8
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH4EMPAMP:
            """Pre-emphasis amplitude."""

            offset = 0
            length = 3
            bit_mask = 7
            default = 0

            @staticmethod
            def validate(value):
                return value in range(8)

    class STATUS:
        """Status Register"""

        address = 0x14

        @staticmethod
        def __str__():
            return "STATUS"

        @staticmethod
        def __int__():
            return 0x14

        class DIS:
            """Mirror of external DIS input state"""

            offset = 3
            length = 1
            bit_mask = 8
            # default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class PORC:
            """Power-on Reset C"""

            offset = 2
            length = 1
            bit_mask = 4
            # default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class PORB:
            """Power-on Reset B"""

            offset = 1
            length = 1
            bit_mask = 2
            # default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class PORA:
            """Power-on Reset A"""

            offset = 0
            length = 1
            bit_mask = 1
            # default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class ID:
        """ID Register"""

        address = 0x15

        @staticmethod
        def __str__():
            return "ID"

        @staticmethod
        def __int__():
            return 0x15

        class CHIPID:
            """Chip type (LDQ10 is 1)"""

            offset = 3
            length = 4
            bit_mask = 240

            @staticmethod
            def validate(value):
                return value in range(16)

        class REVID:
            """Chip revision number (production version is 5)"""

            offset = 0
            length = 4
            bit_mask = 15

            @staticmethod
            def validate(value):
                return value in range(16)

    class UID0:
        """Unique ID[7:0] Register"""

        address = 0x16

        @staticmethod
        def __str__():
            return "UID0"

        @staticmethod
        def __int__():
            return 0x16

        class UIDy:
            """The four registers yield a unique 32-bit chip ID that is set during production testing to allow traceability of chip and module."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    class UID1:
        """Unique ID[15:8] Register"""

        address = 0x17

        @staticmethod
        def __str__():
            return "UID1"

        @staticmethod
        def __int__():
            return 0x17

        class UIDy:
            """The four registers yield a unique 32-bit chip ID that is set during production testing to allow traceability of chip and module."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    class UID2:
        """Unique ID[23:16] Register"""

        address = 0x18

        @staticmethod
        def __str__():
            return "UID2"

        @staticmethod
        def __int__():
            return 0x18

        class UIDy:
            """The four registers yield a unique 32-bit chip ID that is set during production testing to allow traceability of chip and module."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    class UID3:
        """Unique ID[31:24] Register"""

        address = 0x19

        @staticmethod
        def __str__():
            return "UID3"

        @staticmethod
        def __int__():
            return 0x19

        class UIDy:
            """The four registers yield a unique 32-bit chip ID that is set during production testing to allow traceability of chip and module."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    class SEU0:
        """SEU counter [7:0] Register"""

        address = 0x1a

        @staticmethod
        def __str__():
            return "SEU0"

        @staticmethod
        def __int__():
            return 0x1a

        class SEUy:
            """The four registers yield the contents of the SEU counter. Writing any value to any of the four registers clears the counter."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    class SEU1:
        """SEU counter [15:8] Register"""

        address = 0x1b

        @staticmethod
        def __str__():
            return "SEU1"

        @staticmethod
        def __int__():
            return 0x1b

        class SEUy:
            """The four registers yield the contents of the SEU counter. Writing any value to any of the four registers clears the counter."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    class SEU2:
        """SEU counter [23:16] Register"""

        address = 0x1c

        @staticmethod
        def __str__():
            return "SEU2"

        @staticmethod
        def __int__():
            return 0x1c

        class SEUy:
            """The four registers yield the contents of the SEU counter. Writing any value to any of the four registers clears the counter."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    class SEU3:
        """SEU counter [31:24] Register"""

        address = 0x1d

        @staticmethod
        def __str__():
            return "SEU3"

        @staticmethod
        def __int__():
            return 0x1d

        class SEUy:
            """The four registers yield the contents of the SEU counter. Writing any value to any of the four registers clears the counter."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)

    @unique
    class Reg(IntEnum):
        GCR = 0x00
        SDACNT = 0x02
        CH1BIAS = 0x03
        CH1MOD = 0x04
        CH1EMP = 0x05
        CH2BIAS = 0x06
        CH2MOD = 0x07
        CH2EMP = 0x08
        CH3BIAS = 0x09
        CH3MOD = 0x0a
        CH3EMP = 0x0b
        CH4BIAS = 0x0c
        CH4MOD = 0x0d
        CH4EMP = 0x0e
        STATUS = 0x14
        ID = 0x15
        UID0 = 0x16
        UID1 = 0x17
        UID2 = 0x18
        UID3 = 0x19
        SEU0 = 0x1a
        SEU1 = 0x1b
        SEU2 = 0x1c
        SEU3 = 0x1d
